﻿using UnityEngine;
using System.Collections;

public class UnitMotion : MonoBehaviour {

	//public Transform target;
    float speed = 10f;
    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector2(0.0f, 7.0f), step);
    }
}
