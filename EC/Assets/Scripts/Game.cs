﻿using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class Game : MonoBehaviour {

    public Text scoreText;
    private int score;
    public int reloadTime;

    public void UpdateScore()
    {
        scoreText.text = "SCORE: " + score;        
    }
    public void Launch() {
        score++;
        UpdateScore();
    }
    
    public void ResetScore()
    {
        score = 0;
        UpdateScore();
    }

    public void LongTap() {

    }

    
}
