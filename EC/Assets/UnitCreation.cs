﻿using UnityEngine;
using System.Collections;

public class UnitCreation : MonoBehaviour
{

    public GameObject unit;
    public GameObject clone;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(Create());
    }

    IEnumerator Create()
    {
        yield return new WaitForSeconds(0.5f);
        clone = Instantiate(unit, new Vector2(Random.Range(-3.0f, 3.0f), Random.Range(-5.0f, 0.0f)), Quaternion.identity) as GameObject;
        Destroy(clone,1f);
        Repeat();

    }

    void Repeat()
    {
        StartCoroutine(Create());
    }
}
